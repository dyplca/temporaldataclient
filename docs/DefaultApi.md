# DefaultApi

All URIs are relative to *http://localhost:9000/v0.2*

Method | HTTP request | Description
------------- | ------------- | -------------
[**activitiesGet**](DefaultApi.md#activitiesGet) | **GET** /activities | 
[**activitiesPost**](DefaultApi.md#activitiesPost) | **POST** /activities | 
[**activityGet**](DefaultApi.md#activityGet) | **GET** /activity | 


<a name="activitiesGet"></a>
# **activitiesGet**
> ActivitiesResponse activitiesGet(category, fields)



Gets &#x60;Activity&#x60; objects. 

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String category = "category_example"; // String | the name of a category, like Chemicals\\Organic\\Transformation
String fields = "fields_example"; // String | Returns the specific fields of a given activity. Must also specify either id or name. It can be any of the fields definning the activity object like 'unit' or 'tau'. Multiple fields can be given, each separated by a comma (e.g. \"dyplcaId,name,geography\").
try {
    ActivitiesResponse result = apiInstance.activitiesGet(category, fields);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#activitiesGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **category** | **String**| the name of a category, like Chemicals\\Organic\\Transformation | [optional]
 **fields** | **String**| Returns the specific fields of a given activity. Must also specify either id or name. It can be any of the fields definning the activity object like &#39;unit&#39; or &#39;tau&#39;. Multiple fields can be given, each separated by a comma (e.g. \&quot;dyplcaId,name,geography\&quot;). | [optional]

### Return type

[**ActivitiesResponse**](ActivitiesResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

<a name="activitiesPost"></a>
# **activitiesPost**
> ErrorResponse activitiesPost()



Creates a new activity in the db.

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
try {
    ErrorResponse result = apiInstance.activitiesPost();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#activitiesPost");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ErrorResponse**](ErrorResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

<a name="activityGet"></a>
# **activityGet**
> ActivityResponse activityGet(id, name, fields, simaproExport)



Return a specific activity

### Example
```java
// Import classes:
//import io.swagger.client.ApiException;
//import io.swagger.client.api.DefaultApi;


DefaultApi apiInstance = new DefaultApi();
String id = "id_example"; // String | the id of the activity, like 95aa50e0-ea51-4165-a539-6499e43a08c2
String name = "name_example"; // String | the name of the activity
String fields = "fields_example"; // String | Returns the specific fields of a given activity. Must also specify either id or name. It can be any of the fields definning the activity object like 'unit' or 'tau'. Multiple fields can be given, each separated by a comma (e.g. \"dyplcaId,name,geography\").
String simaproExport = "simaproExport_example"; // String | SimaPro v8.4 process naming style.
try {
    ActivityResponse result = apiInstance.activityGet(id, name, fields, simaproExport);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling DefaultApi#activityGet");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **String**| the id of the activity, like 95aa50e0-ea51-4165-a539-6499e43a08c2 | [optional]
 **name** | **String**| the name of the activity | [optional]
 **fields** | **String**| Returns the specific fields of a given activity. Must also specify either id or name. It can be any of the fields definning the activity object like &#39;unit&#39; or &#39;tau&#39;. Multiple fields can be given, each separated by a comma (e.g. \&quot;dyplcaId,name,geography\&quot;). | [optional]
 **simaproExport** | **String**| SimaPro v8.4 process naming style. | [optional]

### Return type

[**ActivityResponse**](ActivityResponse.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

