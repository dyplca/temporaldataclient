
# Activity

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**simaproExport** | **String** |  |  [optional]
**id** | **String** |  |  [optional]
**name** | **String** |  |  [optional]
**geography** | **String** |  |  [optional]
**startDate** | **String** |  |  [optional]
**endDate** | **String** |  |  [optional]
**isicNumber** | **String** |  |  [optional]
**isicName** | **String** |  |  [optional]
**type** | **String** |  |  [optional]
**inheritenceStatus** | **String** |  |  [optional]
**tags** | **String** |  |  [optional]
**group** | **String** |  |  [optional]
**productName** | **String** |  |  [optional]
**unit** | **String** |  |  [optional]
**aposClassification** | **String** |  |  [optional]
**tYears** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**rDays** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**deltaDays** | [**BigDecimal**](BigDecimal.md) |  |  [optional]
**alpha** | **String** |  |  [optional]
**beta** | **String** |  |  [optional]
**fixedCalendar** | **Boolean** |  |  [optional]
**tau** | **String** |  |  [optional]
**continuousSupply** | **Boolean** |  |  [optional]
**market** | **Boolean** |  |  [optional]
**service** | **Boolean** |  |  [optional]
**obs** | **String** |  |  [optional]



